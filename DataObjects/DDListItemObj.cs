﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class DDListItemObj 
    {
        public int dd_det_id { get; set; }

        public string dd_val_code1 { get; set; }

        public string dd_value { get; set; }

        public bool active { get; set; }
    }

    public class DDListHeaderObj 
    {
        public int dd_hdr_id { get; set; }

        public int comp_id { get; set; }

        public string dd_list_name { get; set; }

        public string dd_code1 { get; set; }

        public string dd_code2 { get; set; }

        public string comments { get; set; }

        public bool active { get; set; }
    }

    public class StateDDListItemObj 
    {
        public int state_id { get; set; }

        public string state_code { get; set; }

        public string state_name { get; set; }

    }

    public class CountyDDListItemObj 
    {
        public int county_id { get; set; }

        public string county_code { get; set; }

        public string county_name { get; set; }

    }

    public class CityDDListItemObj 
    {
        public string city_name { get; set; }
    }

    public class CountryDDListItemObj
    {
        public int country_id { get; set; }

        public string country_code { get; set; }

        public string country_name { get; set; }

    }

    public class DDListRequest
    {
        public string p_dd_name { get; set; }
    }

    public class DDStateRequest
    {
        public string scc { get; set; }

        public string search { get; set; }
    }

}
