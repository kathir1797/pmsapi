﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class PropertyObj
    {
        public int? p_comp_id { get; set; }

        public int? p_usr_id { get; set; }

        public int? p_prop_id { get; set; }
    }

    public class AddPropertyRequest
    {
        public int? p_prop_id { get; set; }

        public int? p_comp_id { get; set; }

        public int? p_owner_id { get; set; }

        public string p_prop_name { get; set; }

        public int? p_prop_type { get; set; }

        public int? p_prop_mode { get; set; }

        public int? p_prop_zone { get; set; }

        public int? p_bhk { get; set; }

        public int? p_floor { get; set; }

        public int? p_total_floor { get; set; }

        public int? p_prop_status { get; set; }

        public int? p_prop_size { get; set; }

        public int? p_room_count { get; set; }

        public int?  p_no_of_bath { get; set; }

        public string p_amenities { get; set; }

        public string p_elect_prov { get; set; }

        public string p_elect_ph { get; set; }

        public string p_gas_prov { get; set; }

        public string p_gas_ph { get; set; }

        public string p_water_prov { get; set; }

        public string p_water_ph { get; set; }

        public string p_city { get; set; }

        public int p_county { get; set; }

        public int p_state { get; set; }

        public int p_prop_age { get; set; }

        public int p_country { get; set; }

        public int p_parking_space { get; set; }

        public string p_address { get; set; }

        public string p_address1 { get; set; }

        public string p_zip { get; set; }

        public string p_comments { get; set; }

        public bool p_active { get; set; }

        public int p_usr_id { get; set; }

    }

    public class GetpropertiesRequest
    {
        public int? p_prop_id { get; set; }
        public string p_prop_name { get; set; }
        public string p_owner_name { get; set; }
        public string p_prop_address { get; set; }
        public string p_zip_code { get; set; }
        public int? p_usr_id { get; set; }
        public int? p_comp_id { get; set; }
    }

    public class GetProperyList
    {
        public int prop_id { get; set; }

        public int comp_id { get; set; }

        public string comp_name { get; set; }

        public string prop_name { get; set; }

        public string prop_address { get; set; }

        public string prop_address1 { get; set; }

        public string ppt_type { get; set; }

        public string ppt_mode { get; set; }

        public string ppt_zone { get; set; }

        public string ppt_bhk { get; set; }

        public string ppt_floor { get; set; }

        public int total_floor { get; set; }

        public int? year_built { get; set; }

        public int? deposit_amt { get; set; }

        public int? year_roof_change { get; set; }

        public int? tenanttype { get; set; }

        public int? ppt_face { get; set; }

        public string ppt_status { get; set; }

        public int? prop_size { get; set; }

        public int? parking_space { get; set; }

        public int? no_of_rooms { get; set; }

        public int? no_of_bathroom { get; set; }

        public string other_amenities { get; set; }

        public string electricity_prov { get; set; }

        public string electricity_prov_ph { get; set; }

        public string water_prv { get; set; }

        public string water_prv_phone_no { get; set; }

        public string gas_prov { get; set; }


        public string gas_prov_ph_no { get; set; }

        public string city_name { get; set; }

        public string state_name { get; set; }

        public string county_name { get; set; }

        public string country_name { get; set; }

        public string prop_neigh { get; set; }

        public bool is_negot { get; set; }

        public bool is_avail { get; set; }

        public bool active { get; set; }

        public string prop_zip_code { get; set; }

        public string owner_fullname { get; set; }

        public string usr_fname { get; set; }

        public string prop_age_desc { get; set; }

        public string created_by { get; set; }

        public DateTime created_date { get; set; }

    }
}
