﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class AddCompanyRequest 
    {
        public int p_comp_id { get; set; }

        public int p_comp_type { get; set; }

        public string p_comp_name { get; set; }

        public string p_comp_address { get; set; }

        public string p_comp_address1 { get; set; }

        public int p_country { get; set; }

        public int p_county { get; set; }

        public string p_city { get; set; }

        public int p_state { get; set; }

        public string p_zipcode { get; set; }

        public string p_contact_person { get; set; }

        public string p_comp_phone { get; set; }

        public string p_comp_mail { get; set; }

        public string p_admin_fname { get; set; }

        public string p_admin_lname { get; set; }

        public string p_admin_mname { get; set; }

        public string p_admin_pswd { get; set; }

        public bool p_active { get; set; }

        public int p_usr_id { get; set; }

    }


    public class GetCtyRequest
    {
        public int? p_comp_id { get; set; }
        public int? p_usr_id { get; set; }
        public int? p_level { get; set; }
    }

    public class CompanyObj 
    {
        public int comp_id { get; set; }

        public int comp_type { get; set; }

        public string comp_name { get; set; }

        public string comp_type_desc { get; set; }

        public string comp_address { get; set; }

        public string comp_address1 { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string country_name { get; set; }

        public string county_name { get; set; }

        public string comp_zip { get; set; }

        public string comp_email { get; set; }

        public bool active { get; set; }

        public string admin_full_name { get; set; }

        public string usr_fname { get; set; }

        public string usr_mname { get; set; }

        public string usr_lname { get; set; }

        public int usr_type_desc { get; set; }

        public string comp_phone { get; set; }

        public bool is_comp { get; set; }

        public string usr_pswd { get; set; }
    }
}
