﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class TenantObj
    {
        public int? tenant_id { get; set; }

        public int? comp_id { get; set; }

        public string tnt_fullname { get; set; }

        public bool active { get; set; }

        public string tnt_address { get; set; }

        public string tnt_address1 { get; set; }

        public string tnt_phone { get; set; }

        public string tnt_mail { get; set; }

        public string terminate_rsn { get; set; }

        public string tnt_login { get; set; }

        public string tnt_fname { get; set; }

        public string tnt_mname { get; set; }

        public string tnt_lname { get; set; }

        public string tnt_zip { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string user_type { get; set; }

        public string usr_pswd { get; set; }

        public string tnt_type { get; set; }

    }

    public class AddTenantRequest
    {
        public int? p_tenant_id { get; set; }
        public int? p_comp_id { get; set; }
        public string p_tnt_fname { get; set; }
        public string p_tnt_mname { get; set; }
        public string p_tnt_lname { get; set; }
        public int? p_tnt_type { get; set; }
        public string p_tnt_email { get; set; }
        public string p_tnt_phone { get; set; }
        public bool p_active { get; set; }
        public int? p_usr_id { get; set; }
    }

    public class GetTenantRequest
    {
        public int? p_comp_id { get; set; }

        public int? p_usr_id { get; set; }

        public int? p_tnt_id { get; set; }
    }
}
