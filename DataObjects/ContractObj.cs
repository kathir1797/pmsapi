﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class ContractObj
    {
        public int? tenant_cont_id { get; set; }

        public int? tenant_id { get; set; }

        public string contract_name { get; set; }

        public int prop_id { get; set; }

        public DateTime cont_from { get; set; }

        public DateTime cont_to { get; set; }

        public decimal rent_amt { get; set; }

        public DateTime terminate_date { get; set; }

        public string terminate_rsn { get; set; }

        public bool active { get; set; }

        public string contract_type_desc { get; set; }

        public string terminate_rsn_desc { get; set; }

        public string tnt_fname { get; set; }

        public string usr_mname { get; set; }

        public string usr_lname { get; set; }

        public string tnt_full_name { get; set; }

        public string prop_name { get; set; }

        public string prop_address { get; set; }

        public string comments { get; set; }

    }

    public class AddContractRequest
    {
        public int? p_tenant_cont_id { get; set; }
        public int? p_tenant_id { get; set; }
        public string p_contract_name { get; set; }
        public int? p_prop_id { get; set; }
        public DateTime p_cont_from { get; set; }
        public DateTime p_cont_to { get; set; }
        public float p_rent_amt { get; set; }
        public bool p_active { get; set; }
        public int? p_usr_id { get; set; }
    }

    public class GetContractRequest
    {
        public int? p_comp_id { get; set; }

        public int? p_usr_id { get; set; }

        public int? p_tenant_cont_id { get; set; }
    }

    public class TerminateLeaseRequest
    {
        public int p_tnt_cont_id { get; set; }

        public int p_rsn_id { get; set; }

        public string p_rsn_cmts { get; set; }

        public DateTime p_trm_date { get; set; }

        public int p_usr_id { get; set; }
    }

}
