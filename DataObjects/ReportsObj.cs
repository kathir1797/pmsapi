﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{   

    public class GetRptproperties
    {
        public int? p_usr_id { get; set; }

        public int? p_comp_id { get; set; }
    }

    public class ReportsPrtyObj
    {
        public int? prop_id { get; set; }

        public int? comp_id { get; set; }

        public string prop_name { get; set; }

        public string propstatus { get; set; }

        public string proptype { get; set; }

        public string city_name { get; set; }

        public string state_name { get; set; }

        public string county_name { get; set; }

        public string country_name { get; set; }

        public string owner_fullname { get; set; }

        public string tnt_fullname { get; set; }
    }

    public class ReportsOwnerObj
    {
        public int? owner_id { get; set; }

        public int? comp_id { get; set; }

        public string owner_fullname { get; set; }

        public bool active { get; set; }

        public string owner_address { get; set; }

        public string owner_address1 { get; set; }

        public string owner_phone { get; set; }

        public string owner_mail { get; set; }

        public string owner_login { get; set; }

        public string owner_fname { get; set; }

        public string owner_mname { get; set; }

        public string owner_lname { get; set; }

        public string owner_zip { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string user_type { get; set; }
    }

    public class ReportsTenantObj
    {
        public int? tenant_id { get; set; }

        public int? comp_id { get; set; }

        public string tnt_fullname { get; set; }

        public bool active { get; set; }

        public string tnt_address { get; set; }

        public string tnt_address1 { get; set; }

        public string tnt_phone { get; set; }

        public string tnt_mail { get; set; }

        public string tnt_login { get; set; }

        public string tnt_fname { get; set; }

        public string tnt_mname { get; set; }

        public string tnt_lname { get; set; }

        public string tnt_zip { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string user_type { get; set; }

        public string tnt_type { get; set; }
    }

    public class ReportsContractObj
    {
        public int? tenant_cont_id { get; set; }

        public string contract_name { get; set; }

        public DateTime cont_from { get; set; }

        public DateTime cont_to { get; set; }

        public decimal rent_amt { get; set; }

        public string prop_name { get; set; }

        public string prop_address { get; set; }

        public string prop_zip_code { get; set; }

        public int? tenant_id { get; set; }

        public int? comp_id { get; set; }

        public string tnt_fullname { get; set; }

        public bool active { get; set; }

        public string tnt_address { get; set; }

        public string tnt_phone { get; set; }

        public string tnt_mail { get; set; }

        public string tnt_zip { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string tnt_type { get; set; }

        public string owner_fullname { get; set; }
    }
}
