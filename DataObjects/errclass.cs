﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{

    [Serializable]
    public class errclass
    {
        private string err;
        public string Err
        {
            get { return err; }
            set { err = value; }
        }
    }
}
