﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class ContactObj
    {
        public int? form_id { get; set; }
        public string comp_name { get; set; }
        public string cont_fname { get; set; }
        public string cont_lname { get; set; }
        public string email { get; set; }
        public string ph_no { get; set; }       
    }

    public class AddContactRequest
    {
        public int? p_form_id { get; set; }
        public string p_comp_name { get; set; }
        public string p_fname { get; set; }
        public string p_lname { get; set; }
        public string p_mail { get; set; }
        public string p_phone { get; set; }
    }
}
