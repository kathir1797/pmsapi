﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class PropertyOwnerObj
    {
        public int owner_id { get; set; }

        public int comp_id { get; set; }

        public int prop_id { get; set; }

        public string prop_name { get; set; }

        public string owner_fullname { get; set; }

        public bool active { get; set; }

        public string owner_address { get; set; }

        public string owner_address1 { get; set; }

        public string owner_phone { get; set; }

        public string owner_mail { get; set; }

        public string owner_login { get; set; }

       // public string usr_pswd { get; set; }

        public string owner_fname { get; set; }

        public string owner_mname { get; set; }

        public string owner_lname { get; set; }

        public string owner_zip { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string user_type { get; set; }
    }

    public class GetprtyOwnerRequest
    {
        public int? p_comp_id { get; set; }

        public int? p_usr_id { get; set; }

        public int? p_owner_id { get; set; }
    }

    public class AddprtyOwnerRequest
    {
        public int p_owner_id { get; set; }

        public int p_comp_id { get; set; }

        public int p_prop_id { get; set; }

        public string p_usr_fname { get; set; }

        public string p_usr_mname { get; set; }

        public string p_usr_lname { get; set; }

        public string p_usr_email { get; set; }

        public string p_usr_phone { get; set; }

        public bool p_active { get; set; }

        public int p_usr_id { get; set; }

    }
}
