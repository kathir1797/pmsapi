﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class RootloginRequest
    {
        public string pusr_login { get; set; }
        public string pusr_paswd { get; set; }
    }

    public class UserPwdReset
    {
        public int p_login_usr_id { get; set; }
        public string p_usr_pswd_old { get; set; }
        public string p_usr_pswd_new { get; set; }
        public int p_reset_by_usr_id { get; set; }
    }

    public class CurrentUserObj 
    {
        public int usr_id { get; set; }

        public int comp_id { get; set; }

        public string usr_name { get; set; }

        public bool active { get; set; }

        public string usr_address { get; set; }

        public string usr_address1 { get; set; }

        public string usr_zipcode { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string user_type { get; set; }

        public bool pswd_reset { get; set; }
    }
}
