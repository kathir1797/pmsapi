﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class PropertyManagerObj
    {

        public int usr_id { get; set; }

        public int comp_id { get; set; }

        public string usr_fullname { get; set; }

        public bool active { get; set; }

        public string usr_address { get; set; }

        public string usr_address1 { get; set; }

        public string usr_phone { get; set; }

        public string usr_email { get; set; }

        public string usr_login { get; set; }

        public string usr_pswd { get; set; }

        public string usr_fname { get; set; }

        public string usr_mname { get; set; }

        public string usr_lname { get; set; }

        public string usr_zipcode { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string user_type { get; set; }

    }

    public class GetPrtyMgrRequest
    {
        public int? p_comp_id { get; set; }

        public int? p_usr_id { get; set; }

        public int? p_manager_id { get; set; }

    }

    public class GetPrtyUserRequest
    {
        public int? p_comp_id { get; set; }

        public int? p_usr_id { get; set; }

        public int? p_sel_usr { get; set; }

    }

    public class AddprtymgrRequest
    {

        public int p_manager_id { get; set; }

        public int p_comp_id { get; set; }

        public int p_role { get; set; }

        public string p_usr_fname { get; set; }

        public string p_usr_mname { get; set; }

        public string p_usr_lname { get; set; }

        public string p_usr_email { get; set; }

        public string p_pswd { get; set; }

        public string p_usr_phone { get; set; }

        public bool p_active { get; set; }

        public int p_usr_id { get; set; }

    }

    public class GetPropertyManagerObj
    {
        public int manager_id { get; set; }

        public int comp_id { get; set; }

        public string manager_fullname { get; set; }

        public bool active { get; set; }

        public string manager_address { get; set; }

        public string manager_address1 { get; set; }

        public string manager_phone { get; set; }

        public string manager_mail { get; set; }

        public string manager_login { get; set; }

        public string usr_pswd { get; set; }

        public string manager_fname { get; set; }

        public string manager_mname { get; set; }

        public string manager_lname { get; set; }

        public string manager_zip { get; set; }

        public string state_name { get; set; }

        public string city_name { get; set; }

        public string user_type { get; set; }
    }


}
