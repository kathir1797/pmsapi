﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class ContactsDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public ContactsDal(string constr)
        {
            connString = constr;
        }

        public string AddNewContactForm(AddContactRequest contactObj)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_iu_sales_form";
                {
                    cmd.Parameters.AddWithValue("@p_form_id", contactObj.p_form_id);
                    cmd.Parameters.AddWithValue("@p_comp_name", contactObj.p_comp_name);
                    cmd.Parameters.AddWithValue("@p_fname", contactObj.p_fname);
                    cmd.Parameters.AddWithValue("@p_lname", contactObj.p_lname);
                    cmd.Parameters.AddWithValue("@p_mail", contactObj.p_mail);
                    cmd.Parameters.AddWithValue("@p_phone", contactObj.p_phone);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<ContactObj> GetContactsList()
        {
            List<ContactObj> cpyGetist = new List<ContactObj>();
            ContactObj obj = new ContactObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_sales_form";
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new ContactObj();
                    obj.form_id = string.IsNullOrEmpty(reader["form_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["form_id"]);
                    obj.comp_name = Convert.ToString(reader["comp_name"]);
                    obj.cont_fname = Convert.ToString(reader["cont_fname"]);
                    obj.cont_lname = Convert.ToString(reader["cont_lname"]);
                    obj.email = Convert.ToString(reader["email"]);
                    obj.ph_no = Convert.ToString(reader["ph_no"]);
                    cpyGetist.Add(obj);

                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

    }
}
