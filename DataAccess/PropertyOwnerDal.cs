﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class PropertyOwnerDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public PropertyOwnerDal(string constr)
        {
            connString = constr;
        }

        public string AddNewPropertyOwner(AddprtyOwnerRequest addprtymgr)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_iu_property_owner";
                {
                    cmd.Parameters.AddWithValue("@p_owner_id", addprtymgr.p_owner_id);
                    cmd.Parameters.AddWithValue("@p_comp_id", addprtymgr.p_comp_id);
                    cmd.Parameters.AddWithValue("@p_prop_id", addprtymgr.p_prop_id);
                    cmd.Parameters.AddWithValue("@p_usr_fname", addprtymgr.p_usr_fname);
                    cmd.Parameters.AddWithValue("@p_usr_mname", addprtymgr.p_usr_mname);
                    cmd.Parameters.AddWithValue("@p_usr_lname", addprtymgr.p_usr_lname);
                    cmd.Parameters.AddWithValue("@p_usr_email", addprtymgr.p_usr_email);
                    cmd.Parameters.AddWithValue("@p_usr_phone", addprtymgr.p_usr_phone);
                    cmd.Parameters.AddWithValue("@p_active", addprtymgr.p_active);
                    cmd.Parameters.AddWithValue("@p_usr_id", addprtymgr.p_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<PropertyOwnerObj> GetPropertOwnerList(GetprtyOwnerRequest ownerRequest)
        {
            List<PropertyOwnerObj> cpyGetist = new List<PropertyOwnerObj>();
            PropertyOwnerObj obj = new PropertyOwnerObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_property_owners";
                cmd.Parameters.AddWithValue("@p_comp_id", ownerRequest.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", ownerRequest.p_usr_id);
                cmd.Parameters.AddWithValue("@p_owner_id", ownerRequest.p_owner_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new PropertyOwnerObj();
                    obj.owner_id = string.IsNullOrEmpty(reader["owner_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["owner_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.prop_id = string.IsNullOrEmpty(reader["prop_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["prop_id"]);
                    obj.prop_name = Convert.ToString(reader["prop_name"]);
                    obj.owner_fullname = Convert.ToString(reader["owner_fullname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.owner_address = Convert.ToString(reader["owner_address"]);
                    obj.owner_address1 = Convert.ToString(reader["owner_address1"]);
                    obj.owner_phone = Convert.ToString(reader["owner_phone"]);
                    obj.owner_mail = Convert.ToString(reader["owner_mail"]);
                    obj.owner_login = Convert.ToString(reader["owner_login"]);
                    //obj.usr_pswd = Convert.ToString(reader["usr_pswd"]);
                    obj.owner_fname = Convert.ToString(reader["owner_fname"]);
                    obj.owner_mname = Convert.ToString(reader["owner_mname"]);
                    obj.owner_lname = Convert.ToString(reader["owner_lname"]);
                    obj.owner_zip = Convert.ToString(reader["owner_zip"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.user_type = Convert.ToString(reader["user_type"]);
                    cpyGetist.Add(obj);

                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }
    }
}
