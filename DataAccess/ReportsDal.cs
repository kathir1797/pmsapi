﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class ReportsDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public ReportsDal(string constr)
        {
            connString = constr;
        }

        public List<ReportsPrtyObj> GetReportPropertyList(GetRptproperties getRequest)
        {
            List<ReportsPrtyObj> cpyGetist = new List<ReportsPrtyObj>();
            ReportsPrtyObj obj = new ReportsPrtyObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_rpt_properties";
                cmd.Parameters.AddWithValue("@p_usr_id", getRequest.p_usr_id);
                cmd.Parameters.AddWithValue("@p_comp_id", getRequest.p_comp_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new ReportsPrtyObj();
                    obj.prop_id = string.IsNullOrEmpty(reader["prop_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["prop_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.prop_name = Convert.ToString(reader["prop_name"]);
                    obj.propstatus = Convert.ToString(reader["propstatus"]);
                    obj.proptype = Convert.ToString(reader["proptype"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.county_name = Convert.ToString(reader["county_name"]);
                    obj.country_name = Convert.ToString(reader["country_name"]);
                    obj.owner_fullname = Convert.ToString(reader["owner_fullname"]);
                    obj.tnt_fullname = Convert.ToString(reader["tnt_fullname"]);
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<ReportsOwnerObj> GetReportOwnerList(GetRptproperties getRequest)
        {
            List<ReportsOwnerObj> cpyGetist = new List<ReportsOwnerObj>();
            ReportsOwnerObj obj = new ReportsOwnerObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_rpt_owners";
                cmd.Parameters.AddWithValue("@p_comp_id", getRequest.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getRequest.p_usr_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new ReportsOwnerObj();
                    obj.owner_id = string.IsNullOrEmpty(reader["owner_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["owner_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.owner_fullname = Convert.ToString(reader["owner_fullname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.owner_address = Convert.ToString(reader["owner_address"]);
                    obj.owner_address1 = Convert.ToString(reader["owner_address1"]);
                    obj.owner_phone = Convert.ToString(reader["owner_phone"]);
                    obj.owner_mail = Convert.ToString(reader["owner_mail"]);
                    obj.owner_login = Convert.ToString(reader["owner_login"]);
                    obj.owner_fname = Convert.ToString(reader["owner_fname"]);
                    obj.owner_mname = Convert.ToString(reader["owner_mname"]);
                    obj.owner_lname = Convert.ToString(reader["owner_lname"]);
                    obj.owner_zip = Convert.ToString(reader["owner_zip"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.user_type = Convert.ToString(reader["user_type"]);
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<ReportsTenantObj> GetReportTenantList(GetRptproperties getRequest)
        {
            List<ReportsTenantObj> cpyGetist = new List<ReportsTenantObj>();
            ReportsTenantObj obj = new ReportsTenantObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_rpt_tenant";
                cmd.Parameters.AddWithValue("@p_comp_id", getRequest.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getRequest.p_usr_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new ReportsTenantObj();
                    obj.tenant_id = string.IsNullOrEmpty(reader["tenant_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenant_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.tnt_fullname = Convert.ToString(reader["tnt_fullname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.tnt_address = Convert.ToString(reader["tnt_address"]);
                    obj.tnt_address1 = Convert.ToString(reader["tnt_address1"]);
                    obj.tnt_phone = Convert.ToString(reader["tnt_phone"]);
                    obj.tnt_mail = Convert.ToString(reader["tnt_mail"]);
                    obj.tnt_login = Convert.ToString(reader["tnt_login"]);
                    obj.tnt_fname = Convert.ToString(reader["tnt_fname"]);
                    obj.tnt_fullname = Convert.ToString(reader["tnt_fullname"]);
                    obj.tnt_mname = Convert.ToString(reader["tnt_mname"]);
                    obj.tnt_lname = Convert.ToString(reader["tnt_lname"]);
                    obj.tnt_zip = Convert.ToString(reader["tnt_zip"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.user_type = Convert.ToString(reader["user_type"]);
                    obj.tnt_type = Convert.ToString(reader["tnt_type"]);
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<ReportsContractObj> GetReportContractList(GetRptproperties getRequest)
        {
            List<ReportsContractObj> cpyGetist = new List<ReportsContractObj>();
            ReportsContractObj obj = new ReportsContractObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_rpt_contract";
                cmd.Parameters.AddWithValue("@p_comp_id", getRequest.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getRequest.p_usr_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new ReportsContractObj();
                    obj.tenant_cont_id = string.IsNullOrEmpty(reader["tenant_cont_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenant_cont_id"]);
                    obj.contract_name = Convert.ToString(reader["contract_name"]);
                    obj.cont_from = string.IsNullOrEmpty(reader["cont_from"].ToString()) ? DateTime.Now : Convert.ToDateTime(reader["cont_from"]);
                    obj.cont_to = string.IsNullOrEmpty(reader["cont_to"].ToString()) ? DateTime.Now : Convert.ToDateTime(reader["cont_to"]);
                    obj.rent_amt = Convert.ToDecimal(reader["rent_amt"]);
                    obj.prop_name = Convert.ToString(reader["prop_name"]);
                    obj.prop_address = Convert.ToString(reader["prop_address"]);
                    obj.prop_zip_code = Convert.ToString(reader["prop_zip_code"]);
                    obj.tenant_id = string.IsNullOrEmpty(reader["tenant_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenant_id"]);
                    obj.comp_id = Convert.ToInt32(reader["comp_id"]);
                    obj.tnt_fullname = Convert.ToString(reader["tnt_fullname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.tnt_address = Convert.ToString(reader["tnt_address"]);
                    obj.tnt_phone = Convert.ToString(reader["tnt_phone"]);
                    obj.tnt_mail = Convert.ToString(reader["tnt_mail"]);
                    obj.tnt_zip = Convert.ToString(reader["tnt_zip"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.tnt_type = Convert.ToString(reader["tnt_type"]);
                    obj.owner_fullname = Convert.ToString(reader["owner_fullname"]);
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }
    }
}