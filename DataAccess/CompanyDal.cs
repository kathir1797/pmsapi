﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class CompanyDal
    {   
      
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public CompanyDal(string constr)
        {
            connString = constr;
        }

        public string AddNewCompany(AddCompanyRequest companyObj)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_iu_company";
                {
                    cmd.Parameters.AddWithValue("@p_comp_id", companyObj.p_comp_id);
                    cmd.Parameters.AddWithValue("@p_comp_type", companyObj.p_comp_type);
                    cmd.Parameters.AddWithValue("@p_comp_name", companyObj.p_comp_name);
                    cmd.Parameters.AddWithValue("@p_comp_address", companyObj.p_comp_address);
                    cmd.Parameters.AddWithValue("@p_comp_address1", companyObj.p_comp_address1);
                    cmd.Parameters.AddWithValue("@p_country", companyObj.p_country);
                    cmd.Parameters.AddWithValue("@p_county", companyObj.p_county);
                    cmd.Parameters.AddWithValue("@p_city", companyObj.p_city);
                    cmd.Parameters.AddWithValue("@p_state", companyObj.p_state);
                    cmd.Parameters.AddWithValue("@p_zipcode", companyObj.p_zipcode);
                    cmd.Parameters.AddWithValue("@p_contact_person", companyObj.p_contact_person);
                    cmd.Parameters.AddWithValue("@p_comp_phone", companyObj.p_comp_phone);
                    cmd.Parameters.AddWithValue("@p_comp_mail", companyObj.p_comp_mail);
                    cmd.Parameters.AddWithValue("@p_admin_fname", companyObj.p_admin_fname);
                    cmd.Parameters.AddWithValue("@p_admin_lname", companyObj.p_admin_lname);
                    cmd.Parameters.AddWithValue("@p_admin_mname", companyObj.p_admin_mname);
                    cmd.Parameters.AddWithValue("@p_admin_pswd", companyObj.p_admin_pswd);
                    cmd.Parameters.AddWithValue("@p_active", companyObj.p_active);
                    cmd.Parameters.AddWithValue("@p_usr_id", companyObj.p_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<CompanyObj> GetCompanies(GetCtyRequest ctyRequest)
        {
            List<CompanyObj> cpyGetist = new List<CompanyObj>();
            CompanyObj obj = new CompanyObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_company";
                cmd.Parameters.AddWithValue("@p_comp_id", ctyRequest.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", ctyRequest.p_usr_id);
                cmd.Parameters.AddWithValue("@p_level", ctyRequest.p_level);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new CompanyObj();
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.comp_type = string.IsNullOrEmpty(reader["comp_type"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_type"]);
                    obj.comp_name = Convert.ToString(reader["comp_name"]);
                    obj.comp_type_desc = Convert.ToString(reader["comp_type_desc"]);
                    obj.comp_address = Convert.ToString(reader["comp_address"]);
                    obj.comp_address1 = Convert.ToString(reader["comp_address1"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.country_name = Convert.ToString(reader["country_name"]);
                    obj.county_name = Convert.ToString(reader["county_name"]);
                    obj.comp_zip = Convert.ToString(reader["comp_zip"]);
                    obj.comp_email = Convert.ToString(reader["comp_email"]);
                    obj.admin_full_name = Convert.ToString(reader["admin_full_name"]);
                    obj.usr_fname = Convert.ToString(reader["usr_fname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.usr_mname = Convert.ToString(reader["usr_mname"]);
                    obj.usr_lname = Convert.ToString(reader["usr_lname"]);
                    obj.usr_type_desc = Convert.ToInt32(reader["usr_type_desc"]);
                    obj.comp_phone = Convert.ToString(reader["comp_phone"]);
                    obj.is_comp = Convert.ToBoolean(reader["is_comp"]);
                    obj.usr_pswd = Convert.ToString(reader["usr_pswd"]);                    
                    cpyGetist.Add(obj);
                  
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

    }

}
