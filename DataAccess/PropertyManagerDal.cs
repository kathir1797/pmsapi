﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class PropertyManagerDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public PropertyManagerDal(string constr)
        {
            connString = constr;
        }

        public string AddNewPropertyManager(AddprtymgrRequest addprtymgr)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_iu_user";
                {
                    cmd.Parameters.AddWithValue("@p_manager_id", addprtymgr.p_manager_id);
                    cmd.Parameters.AddWithValue("@p_comp_id", addprtymgr.p_comp_id);
                    cmd.Parameters.AddWithValue("@p_role", addprtymgr.p_role);
                    cmd.Parameters.AddWithValue("@p_usr_fname", addprtymgr.p_usr_fname);
                    cmd.Parameters.AddWithValue("@p_usr_mname", addprtymgr.p_usr_mname);
                    cmd.Parameters.AddWithValue("@p_usr_lname", addprtymgr.p_usr_lname);
                    cmd.Parameters.AddWithValue("@p_usr_email", addprtymgr.p_usr_email);
                    cmd.Parameters.AddWithValue("@p_pswd", addprtymgr.p_pswd);
                    cmd.Parameters.AddWithValue("@p_usr_phone", addprtymgr.p_usr_phone);
                    cmd.Parameters.AddWithValue("@p_active", addprtymgr.p_active);
                    cmd.Parameters.AddWithValue("@p_usr_id", addprtymgr.p_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<PropertyManagerObj> GetAllUsers(GetPrtyUserRequest getPrtyMgr)
        {
            List<PropertyManagerObj> cpyGetist = new List<PropertyManagerObj>();
            PropertyManagerObj obj = new PropertyManagerObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_users";
                cmd.Parameters.AddWithValue("@p_comp_id", getPrtyMgr.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getPrtyMgr.p_usr_id);
                cmd.Parameters.AddWithValue("@p_sel_usr", getPrtyMgr.p_usr_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new PropertyManagerObj();
                    obj.usr_id = string.IsNullOrEmpty(reader["usr_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["usr_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.usr_fullname = Convert.ToString(reader["usr_fullname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.usr_address = Convert.ToString(reader["usr_address"]);
                    obj.usr_address1 = Convert.ToString(reader["usr_address1"]);
                    obj.usr_phone = Convert.ToString(reader["usr_phone"]);
                    obj.usr_email = Convert.ToString(reader["usr_email"]);
                    obj.usr_login = Convert.ToString(reader["usr_login"]);
                    obj.usr_pswd = Convert.ToString(reader["usr_pswd"]);
                    obj.usr_fname = Convert.ToString(reader["usr_fname"]);
                    obj.usr_mname = Convert.ToString(reader["usr_mname"]);
                    obj.usr_lname = Convert.ToString(reader["usr_lname"]);
                    obj.usr_zipcode = Convert.ToString(reader["usr_zipcode"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.user_type = Convert.ToString(reader["user_type"]);
                    cpyGetist.Add(obj);

                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<GetPropertyManagerObj> GetAllPropertyManager(GetPrtyMgrRequest getPrtyMgr)
        {
            List<GetPropertyManagerObj> cpyGetist = new List<GetPropertyManagerObj>();
            GetPropertyManagerObj obj = new GetPropertyManagerObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_property_managers";
                cmd.Parameters.AddWithValue("@p_comp_id", getPrtyMgr.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getPrtyMgr.p_usr_id);
                cmd.Parameters.AddWithValue("@p_manager_id", getPrtyMgr.p_manager_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new GetPropertyManagerObj();
                    obj.manager_id = string.IsNullOrEmpty(reader["manager_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["manager_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.manager_fullname = Convert.ToString(reader["manager_fullname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.manager_address = Convert.ToString(reader["manager_address"]);
                    obj.manager_address1 = Convert.ToString(reader["manager_address1"]);
                    obj.manager_phone = Convert.ToString(reader["manager_phone"]);
                    obj.manager_mail = Convert.ToString(reader["manager_mail"]);
                    obj.manager_login = Convert.ToString(reader["manager_login"]);
                    obj.usr_pswd = Convert.ToString(reader["usr_pswd"]);
                    obj.manager_fname = Convert.ToString(reader["manager_fname"]);
                    obj.manager_mname = Convert.ToString(reader["manager_mname"]);
                    obj.manager_lname = Convert.ToString(reader["manager_lname"]);
                    obj.manager_zip = Convert.ToString(reader["manager_zip"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.user_type = Convert.ToString(reader["user_type"]);
                    cpyGetist.Add(obj);

                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

    }

}