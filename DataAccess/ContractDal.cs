﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class ContractDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public ContractDal(string constr)
        {
            connString = constr;
        }

        public string AddNewContractForm(AddContractRequest addContract)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_iu_contract";
                {
                    cmd.Parameters.AddWithValue("@p_tenant_cont_id", addContract.p_tenant_cont_id);
                    cmd.Parameters.AddWithValue("@p_tenant_id", addContract.p_tenant_id);
                    cmd.Parameters.AddWithValue("@p_contract_name", addContract.p_contract_name);
                    cmd.Parameters.AddWithValue("@p_prop_id", addContract.p_prop_id);
                    cmd.Parameters.AddWithValue("@p_cont_from", addContract.p_cont_from);
                    cmd.Parameters.AddWithValue("@p_cont_to", addContract.p_cont_to);
                    cmd.Parameters.AddWithValue("@p_rent_amt", addContract.p_rent_amt);
                    cmd.Parameters.AddWithValue("@p_active", addContract.p_active);
                    cmd.Parameters.AddWithValue("@p_usr_id", addContract.p_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }


        public List<ContractObj> GetContractList(GetContractRequest getRequest)
        {
            List<ContractObj> cpyGetist = new List<ContractObj>();
            ContractObj obj = new ContractObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_contract";
                cmd.Parameters.AddWithValue("@p_comp_id", getRequest.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getRequest.p_usr_id);
                cmd.Parameters.AddWithValue("@p_tenant_cont_id", getRequest.p_tenant_cont_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new ContractObj();
                    obj.tenant_cont_id = string.IsNullOrEmpty(reader["tenant_cont_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenant_cont_id"]);
                    obj.tenant_id = string.IsNullOrEmpty(reader["tenant_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenant_id"]);
                    obj.contract_name = Convert.ToString(reader["contract_name"]);
                    obj.prop_id = string.IsNullOrEmpty(reader["prop_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["prop_id"]);
                    obj.cont_from = string.IsNullOrEmpty(reader["cont_from"].ToString()) ? DateTime.Now : Convert.ToDateTime(reader["cont_from"]);
                    obj.cont_to = string.IsNullOrEmpty(reader["cont_to"].ToString()) ? DateTime.Now : Convert.ToDateTime(reader["cont_to"]);
                    obj.rent_amt = string.IsNullOrEmpty(reader["rent_amt"].ToString()) ? (decimal)0 : Convert.ToInt32(reader["rent_amt"]);
                    obj.terminate_date = string.IsNullOrEmpty(reader["terminate_date"].ToString()) ? DateTime.Now : Convert.ToDateTime(reader["terminate_date"]);
                    obj.terminate_rsn = Convert.ToString(reader["terminate_rsn"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.contract_type_desc = Convert.ToString(reader["contract_type_desc"]);
                    obj.terminate_rsn_desc = Convert.ToString(reader["terminate_rsn_desc"]);
                    obj.tnt_fname = Convert.ToString(reader["tnt_fname"]);
                    obj.usr_mname = Convert.ToString(reader["usr_mname"]);
                    obj.usr_lname = Convert.ToString(reader["usr_lname"]);
                    obj.tnt_full_name = Convert.ToString(reader["tnt_full_name"]);
                    obj.prop_name = Convert.ToString(reader["prop_name"]);
                    obj.prop_address = Convert.ToString(reader["prop_address"]);
                    obj.comments = Convert.ToString(reader["comments"]);
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public string GetTerminateLease(TerminateLeaseRequest terminateLease)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_terminate_lease";
                {
                    cmd.Parameters.AddWithValue("@p_tnt_cont_id", terminateLease.p_tnt_cont_id);
                    cmd.Parameters.AddWithValue("@p_rsn_id", terminateLease.p_rsn_id);
                    cmd.Parameters.AddWithValue("@p_rsn_cmts", terminateLease.p_rsn_cmts);
                    cmd.Parameters.AddWithValue("@p_trm_date", terminateLease.p_trm_date);
                    cmd.Parameters.AddWithValue("@p_usr_id", terminateLease.p_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }
    }
}