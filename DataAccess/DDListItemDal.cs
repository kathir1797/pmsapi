﻿using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class DDListItemDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public DDListItemDal(string constr)
        {
            connString = constr;
        }
        public List<DDListHeaderObj> GetDropDownHeaderList()
        {
            List<DDListHeaderObj> cpyGetist = new List<DDListHeaderObj>();
            DDListHeaderObj obj = new DDListHeaderObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM pms.tbl_dd_header;";
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new DDListHeaderObj();
                    obj.dd_hdr_id = Convert.ToInt32(reader["dd_hdr_id"]);
                    obj.comp_id = Convert.ToInt32(reader["comp_id"]);
                    obj.dd_list_name = Convert.ToString(reader["dd_list_name"]);
                    obj.dd_code1 = Convert.ToString(reader["dd_code1"]);
                    obj.dd_code2 = Convert.ToString(reader["dd_code2"]);
                    obj.comments = Convert.ToString(reader["comments"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<DDListItemObj> SelectDDItem(DDListRequest DDListName)
        {
            List<DDListItemObj> cpyGetist = new List<DDListItemObj>();
            DDListItemObj obj = new DDListItemObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_get_dd_list";
                cmd.Parameters.AddWithValue("@p_dd_name", DDListName.p_dd_name);                
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new DDListItemObj();
                    obj.dd_det_id = Convert.ToInt32(reader["dd_det_id"]);
                    obj.dd_val_code1 = Convert.ToString(reader["dd_val_code1"]);
                    obj.dd_value = Convert.ToString(reader["dd_value"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                   
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<StateDDListItemObj> SelectStateDDItem(DDStateRequest dStateRequest)
        {
            List<StateDDListItemObj> cpyGetist = new List<StateDDListItemObj>();
            StateDDListItemObj obj = new StateDDListItemObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_dd_scc_list";
                cmd.Parameters.AddWithValue("@scc", dStateRequest.scc);
                cmd.Parameters.AddWithValue("@search", dStateRequest.search);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new StateDDListItemObj();

                    obj.state_id = Convert.ToInt32(reader["state_id"]);
                    obj.state_code = Convert.ToString(reader["state_code"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);

                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<CityDDListItemObj> SelectCityDDItem(DDStateRequest dStateRequest)
        {
            List<CityDDListItemObj> cpyGetist = new List<CityDDListItemObj>();
            CityDDListItemObj obj = new CityDDListItemObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_dd_scc_list";
                cmd.Parameters.AddWithValue("@scc", dStateRequest.scc);
                cmd.Parameters.AddWithValue("@search", dStateRequest.search);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new CityDDListItemObj();
                    obj.city_name = Convert.ToString(reader["city_name"]);

                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<CountyDDListItemObj> SelectCountyDDItem(DDStateRequest dStateRequest)
        {
            List<CountyDDListItemObj> cpyGetist = new List<CountyDDListItemObj>();
            CountyDDListItemObj obj = new CountyDDListItemObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_dd_scc_list";
                cmd.Parameters.AddWithValue("@scc", dStateRequest.scc);
                cmd.Parameters.AddWithValue("@search", dStateRequest.search);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new CountyDDListItemObj();

                    obj.county_id = Convert.ToInt32(reader["county_id"]);
                    obj.county_code = Convert.ToString(reader["county_code"]);
                    obj.county_name = Convert.ToString(reader["county_name"]);

                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<CountryDDListItemObj> SelectCountryDDItem(DDStateRequest dStateRequest)
        {
            List<CountryDDListItemObj> cpyGetist = new List<CountryDDListItemObj>();
            CountryDDListItemObj obj = new CountryDDListItemObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_dd_scc_list";
                cmd.Parameters.AddWithValue("@scc", dStateRequest.scc);
                cmd.Parameters.AddWithValue("@search", "");
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new CountryDDListItemObj();

                    obj.country_id = Convert.ToInt32(reader["country_id"]);
                    obj.country_code = Convert.ToString(reader["country_code"]);
                    obj.country_name = Convert.ToString(reader["country_name"]);

                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }
    }
}
