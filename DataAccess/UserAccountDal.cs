﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class UserAccountDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;
        public UserAccountDal(string constr)
        {
            connString = constr;
        }

        public CurrentUserObj RootUserLogin(RootloginRequest rootlogin)
        {
            CurrentUserObj obj = new CurrentUserObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_validate_user_login";
                cmd.Parameters.AddWithValue("@pusr_login", rootlogin.pusr_login);
                cmd.Parameters.AddWithValue("@pusr_paswd", rootlogin.pusr_paswd);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new CurrentUserObj();
                    obj.usr_id = string.IsNullOrEmpty(reader["usr_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["usr_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.usr_name = Convert.ToString(reader["usr_name"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.usr_address = Convert.ToString(reader["usr_address"]);
                    obj.usr_address1 = Convert.ToString(reader["usr_address1"]);
                    obj.usr_zipcode = Convert.ToString(reader["usr_zipcode"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.user_type = Convert.ToString(reader["user_type"]);
                    obj.pswd_reset = Convert.ToBoolean(reader["pswd_reset"]);
                }

                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public string UserPasswordReset(UserPwdReset pwdReset)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_pswd_reset";
                {
                    cmd.Parameters.AddWithValue("@p_login_usr_id", pwdReset.p_login_usr_id);
                    cmd.Parameters.AddWithValue("@p_usr_pswd_old", pwdReset.p_usr_pswd_old);
                    cmd.Parameters.AddWithValue("@p_usr_pswd_new", pwdReset.p_usr_pswd_new);
                    cmd.Parameters.AddWithValue("@p_reset_by_usr_id", pwdReset.p_reset_by_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }
    }
}
