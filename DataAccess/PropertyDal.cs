﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    public class PropertyDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public PropertyDal(string constr)
        {
            connString = constr;
        }

        public string AddNewProperty(AddPropertyRequest propertyRequest)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_iu_property";
                {
                    cmd.Parameters.AddWithValue("@p_prop_id", propertyRequest.p_prop_id);
                    cmd.Parameters.AddWithValue("@p_comp_id", propertyRequest.p_comp_id);
                    cmd.Parameters.AddWithValue("@p_owner_id", propertyRequest.p_owner_id);
                    cmd.Parameters.AddWithValue("@p_prop_name", propertyRequest.p_prop_name);
                    cmd.Parameters.AddWithValue("@p_prop_type", propertyRequest.p_prop_type);
                    cmd.Parameters.AddWithValue("@p_prop_mode", propertyRequest.p_prop_mode);
                    cmd.Parameters.AddWithValue("@p_prop_zone", propertyRequest.p_prop_zone);
                    cmd.Parameters.AddWithValue("@p_bhk", propertyRequest.p_bhk);
                    cmd.Parameters.AddWithValue("@p_floor", propertyRequest.p_floor);
                    cmd.Parameters.AddWithValue("@p_total_floor", propertyRequest.p_total_floor);
                    cmd.Parameters.AddWithValue("@p_prop_status", propertyRequest.p_prop_status);
                    cmd.Parameters.AddWithValue("@p_prop_size", propertyRequest.p_prop_size);
                    cmd.Parameters.AddWithValue("@p_room_count", propertyRequest.p_room_count);
                    cmd.Parameters.AddWithValue("@p_no_of_bath", propertyRequest.p_no_of_bath);
                    cmd.Parameters.AddWithValue("@p_amenities", propertyRequest.p_amenities);
                    cmd.Parameters.AddWithValue("@p_elect_prov", propertyRequest.p_elect_prov);
                    cmd.Parameters.AddWithValue("@p_elect_ph", propertyRequest.p_elect_ph);
                    cmd.Parameters.AddWithValue("@p_gas_prov", propertyRequest.p_gas_prov);
                    cmd.Parameters.AddWithValue("@p_gas_ph", propertyRequest.p_gas_ph);
                    cmd.Parameters.AddWithValue("@p_water_prov", propertyRequest.p_water_prov);
                    cmd.Parameters.AddWithValue("@p_water_ph", propertyRequest.p_water_ph);
                    cmd.Parameters.AddWithValue("@p_city", propertyRequest.p_city);
                    cmd.Parameters.AddWithValue("@p_county", propertyRequest.p_county);
                    cmd.Parameters.AddWithValue("@p_state", propertyRequest.p_state);
                    cmd.Parameters.AddWithValue("@p_prop_age", propertyRequest.p_prop_age);
                    cmd.Parameters.AddWithValue("@p_country", propertyRequest.p_country);
                    cmd.Parameters.AddWithValue("@p_parking_space", propertyRequest.p_parking_space);
                    cmd.Parameters.AddWithValue("@p_address", propertyRequest.p_address);
                    cmd.Parameters.AddWithValue("@p_address1", propertyRequest.p_address1);
                    cmd.Parameters.AddWithValue("@p_zip", propertyRequest.p_zip);
                    cmd.Parameters.AddWithValue("@p_comments", propertyRequest.p_comments);
                    cmd.Parameters.AddWithValue("@p_active", propertyRequest.p_active);
                    cmd.Parameters.AddWithValue("@p_usr_id", propertyRequest.p_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<GetProperyList> GetProperties(PropertyObj getSearchprty)
        {
            List<GetProperyList> cpyGetist = new List<GetProperyList>();
            GetProperyList obj = new GetProperyList();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_property";
                cmd.Parameters.AddWithValue("@p_comp_id", getSearchprty.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getSearchprty.p_usr_id);
                cmd.Parameters.AddWithValue("@p_prop_id", getSearchprty.p_prop_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new GetProperyList();
                    obj.prop_id = string.IsNullOrEmpty(reader["prop_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["prop_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.comp_name = Convert.ToString(reader["comp_name"]);
                    obj.prop_name = Convert.ToString(reader["prop_name"]);
                    obj.prop_address = Convert.ToString(reader["prop_address"]);
                    obj.prop_address1 = Convert.ToString(reader["prop_address1"]);
                    obj.ppt_type = Convert.ToString(reader["ppt_type"]);
                    obj.ppt_mode = Convert.ToString(reader["ppt_mode"]);
                    obj.ppt_zone = Convert.ToString(reader["ppt_zone"]);
                    obj.ppt_bhk = Convert.ToString(reader["ppt_bhk"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.ppt_floor = Convert.ToString(reader["ppt_floor"]);
                    obj.total_floor = Convert.ToInt32(reader["total_floor"]);
                    obj.year_built = string.IsNullOrEmpty(reader["year_built"].ToString()) ? (int)0 : Convert.ToInt32(reader["year_built"]);
                    obj.deposit_amt = string.IsNullOrEmpty(reader["deposit_amt"].ToString()) ? (int)0 : Convert.ToInt32(reader["deposit_amt"]);
                    obj.year_roof_change = string.IsNullOrEmpty(reader["year_roof_change"].ToString()) ? (int)0 : Convert.ToInt32(reader["year_roof_change"]);
                    obj.tenanttype = string.IsNullOrEmpty(reader["tenanttype"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenanttype"]);
                    obj.ppt_face = string.IsNullOrEmpty(reader["ppt_face"].ToString()) ? (int)0 : Convert.ToInt32(reader["ppt_face"]);
                    obj.ppt_status = Convert.ToString(reader["ppt_status"]);
                    obj.prop_size = string.IsNullOrEmpty(reader["prop_size"].ToString()) ? (int)0 : Convert.ToInt32(reader["prop_size"]);
                    obj.parking_space = string.IsNullOrEmpty(reader["parking_space"].ToString()) ? (int)0 : Convert.ToInt32(reader["parking_space"]);
                    obj.no_of_rooms = string.IsNullOrEmpty(reader["no_of_rooms"].ToString()) ? (int)0 : Convert.ToInt32(reader["no_of_rooms"]);
                    obj.no_of_bathroom = string.IsNullOrEmpty(reader["no_of_bathroom"].ToString()) ? (int)0 : Convert.ToInt32(reader["no_of_bathroom"]);
                    obj.other_amenities = Convert.ToString(reader["other_amenities"]);
                    obj.electricity_prov = Convert.ToString(reader["electricity_prov"]);
                    obj.electricity_prov_ph = Convert.ToString(reader["electricity_prov_ph"]);
                    obj.water_prv = Convert.ToString(reader["water_prv"]);
                    obj.water_prv_phone_no = Convert.ToString(reader["water_prv_phone_no"]);
                    obj.gas_prov = Convert.ToString(reader["gas_prov"]);
                    obj.gas_prov_ph_no = Convert.ToString(reader["gas_prov_ph_no"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.county_name = Convert.ToString(reader["county_name"]);
                    obj.country_name = Convert.ToString(reader["country_name"]);
                    obj.prop_neigh = Convert.ToString(reader["prop_neigh"]);
                    obj.is_negot = Convert.ToBoolean(reader["is_negot"]);
                    obj.is_avail = Convert.ToBoolean(reader["is_avail"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.prop_zip_code = Convert.ToString(reader["prop_zip_code"]);
                    obj.owner_fullname = Convert.ToString(reader["owner_fullname"]);
                    obj.usr_fname = Convert.ToString(reader["usr_fname"]);
                    obj.prop_age_desc = Convert.ToString(reader["prop_age_desc"]);
                    obj.created_by = Convert.ToString(reader["created_by"]);
                    obj.created_date = string.IsNullOrEmpty(reader["created_date"].ToString()) ? DateTime.Now : Convert.ToDateTime(reader["created_date"]);
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
             catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

        public List<GetProperyList> GetSearchProperties(GetpropertiesRequest getCompaniesRequest)
        {
            List<GetProperyList> cpyGetist = new List<GetProperyList>();
            GetProperyList obj = new GetProperyList();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_search_property";
                cmd.Parameters.AddWithValue("@p_prop_id", getCompaniesRequest.p_prop_id);
                cmd.Parameters.AddWithValue("@p_prop_name", getCompaniesRequest.p_prop_name);
                cmd.Parameters.AddWithValue("@p_owner_name", getCompaniesRequest.p_owner_name);
                cmd.Parameters.AddWithValue("@p_prop_address", getCompaniesRequest.p_prop_address);
                cmd.Parameters.AddWithValue("@p_zip_code", getCompaniesRequest.p_zip_code);
                cmd.Parameters.AddWithValue("@p_usr_id", getCompaniesRequest.p_usr_id);
                cmd.Parameters.AddWithValue("@p_comp_id", getCompaniesRequest.p_comp_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new GetProperyList();
                    obj.prop_id = string.IsNullOrEmpty(reader["prop_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["prop_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.comp_name = Convert.ToString(reader["comp_name"]);
                    obj.prop_name = Convert.ToString(reader["prop_name"]);
                    obj.prop_address = Convert.ToString(reader["prop_address"]);
                    obj.prop_address1 = Convert.ToString(reader["prop_address1"]);
                    obj.ppt_type = Convert.ToString(reader["ppt_type"]);
                    obj.ppt_mode = Convert.ToString(reader["ppt_mode"]);
                    obj.ppt_zone = Convert.ToString(reader["ppt_zone"]);
                    obj.ppt_bhk = Convert.ToString(reader["ppt_bhk"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.ppt_floor = Convert.ToString(reader["ppt_floor"]);
                    obj.total_floor = Convert.ToInt32(reader["total_floor"]);
                    obj.year_built = string.IsNullOrEmpty(reader["year_built"].ToString()) ? (int)0 : Convert.ToInt32(reader["year_built"]);
                    obj.deposit_amt = string.IsNullOrEmpty(reader["deposit_amt"].ToString()) ? (int)0 : Convert.ToInt32(reader["deposit_amt"]);
                    obj.year_roof_change = string.IsNullOrEmpty(reader["year_roof_change"].ToString()) ? (int)0 : Convert.ToInt32(reader["year_roof_change"]);
                    obj.tenanttype = string.IsNullOrEmpty(reader["tenanttype"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenanttype"]);
                    obj.ppt_face = string.IsNullOrEmpty(reader["ppt_face"].ToString()) ? (int)0 : Convert.ToInt32(reader["ppt_face"]);
                    obj.ppt_status = Convert.ToString(reader["ppt_status"]);
                    obj.prop_size = string.IsNullOrEmpty(reader["prop_size"].ToString()) ? (int)0 : Convert.ToInt32(reader["prop_size"]);
                    obj.parking_space = string.IsNullOrEmpty(reader["parking_space"].ToString()) ? (int)0 : Convert.ToInt32(reader["parking_space"]);
                    obj.no_of_rooms = string.IsNullOrEmpty(reader["no_of_rooms"].ToString()) ? (int)0 : Convert.ToInt32(reader["no_of_rooms"]);
                    obj.no_of_bathroom = string.IsNullOrEmpty(reader["no_of_bathroom"].ToString()) ? (int)0 : Convert.ToInt32(reader["no_of_bathroom"]);
                    obj.other_amenities = Convert.ToString(reader["other_amenities"]);
                    obj.electricity_prov = Convert.ToString(reader["electricity_prov"]);
                    obj.electricity_prov_ph = Convert.ToString(reader["electricity_prov_ph"]);
                    obj.water_prv = Convert.ToString(reader["water_prv"]);
                    obj.water_prv_phone_no = Convert.ToString(reader["water_prv_phone_no"]);
                    obj.gas_prov = Convert.ToString(reader["gas_prov"]);
                    obj.gas_prov_ph_no = Convert.ToString(reader["gas_prov_ph_no"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.county_name = Convert.ToString(reader["county_name"]);
                    obj.country_name = Convert.ToString(reader["country_name"]);
                    obj.prop_neigh = Convert.ToString(reader["prop_neigh"]);
                    obj.is_negot = Convert.ToBoolean(reader["is_negot"]);
                    obj.is_avail = Convert.ToBoolean(reader["is_avail"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.prop_zip_code = Convert.ToString(reader["prop_zip_code"]);
                    obj.owner_fullname = Convert.ToString(reader["owner_fullname"]);
                    obj.usr_fname = Convert.ToString(reader["usr_fname"]);
                    obj.created_by = Convert.ToString(reader["created_by"]);
                    obj.created_date = string.IsNullOrEmpty(reader["created_date"].ToString()) ? DateTime.Now : Convert.ToDateTime(reader["created_date"]);
                    cpyGetist.Add(obj);

                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }
    }
}
