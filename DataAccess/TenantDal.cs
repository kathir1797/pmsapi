﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PMSAPI
{
    [Serializable]
    public class TenantDal
    {
        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        public string connString = string.Empty;

        public TenantDal(string constr)
        {
            connString = constr;
        }

        public string AddNewTenantForm(AddTenantRequest addTenant)
        {
            Int64 RecordsAffected = 0;
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_iu_tenant";
                {
                    cmd.Parameters.AddWithValue("@p_tenant_id", addTenant.p_tenant_id);
                    cmd.Parameters.AddWithValue("@p_comp_id", addTenant.p_comp_id);
                    cmd.Parameters.AddWithValue("@p_tnt_fname", addTenant.p_tnt_fname);
                    cmd.Parameters.AddWithValue("@p_tnt_mname", addTenant.p_tnt_mname);
                    cmd.Parameters.AddWithValue("@p_tnt_lname", addTenant.p_tnt_lname);
                    cmd.Parameters.AddWithValue("@p_tnt_type", addTenant.p_tnt_type);
                    cmd.Parameters.AddWithValue("@p_tnt_email", addTenant.p_tnt_email);
                    cmd.Parameters.AddWithValue("@p_tnt_phone", addTenant.p_tnt_phone);
                    cmd.Parameters.AddWithValue("@p_active", addTenant.p_active);
                    cmd.Parameters.AddWithValue("@p_usr_id", addTenant.p_usr_id);
                    RecordsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Dispose();
                }
                return "0";
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }


        public List<TenantObj> GetTenantList(GetTenantRequest getRequest)
        {
            List<TenantObj> cpyGetist = new List<TenantObj>();
            TenantObj obj = new TenantObj();
            try
            {
                con = new MySqlConnection(connString);

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure; cmd.CommandTimeout = 200;
                cmd.CommandText = "sp_sel_tenant";
                cmd.Parameters.AddWithValue("@p_comp_id", getRequest.p_comp_id);
                cmd.Parameters.AddWithValue("@p_usr_id", getRequest.p_usr_id);
                cmd.Parameters.AddWithValue("@p_tnt_id", getRequest.p_tnt_id);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    obj = new TenantObj();
                    obj.tenant_id = string.IsNullOrEmpty(reader["tenant_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["tenant_id"]);
                    obj.comp_id = string.IsNullOrEmpty(reader["comp_id"].ToString()) ? (int)0 : Convert.ToInt32(reader["comp_id"]);
                    obj.tnt_fullname = Convert.ToString(reader["tnt_fullname"]);
                    obj.active = Convert.ToBoolean(reader["active"]);
                    obj.tnt_address = Convert.ToString(reader["tnt_address"]);
                    obj.tnt_address1 = Convert.ToString(reader["tnt_address1"]);
                    obj.tnt_phone = Convert.ToString(reader["tnt_phone"]);
                    obj.tnt_mail = Convert.ToString(reader["tnt_mail"]);
                    obj.tnt_login = Convert.ToString(reader["tnt_login"]);
                    obj.tnt_fname = Convert.ToString(reader["tnt_fname"]);
                    obj.tnt_mname = Convert.ToString(reader["tnt_mname"]);
                    obj.tnt_lname = Convert.ToString(reader["tnt_lname"]);
                    obj.tnt_zip = Convert.ToString(reader["tnt_zip"]);
                    obj.state_name = Convert.ToString(reader["state_name"]);
                    obj.city_name = Convert.ToString(reader["city_name"]);
                    obj.user_type = Convert.ToString(reader["user_type"]);
                    obj.usr_pswd = Convert.ToString(reader["usr_pswd"]);
                    obj.tnt_type = Convert.ToString(reader["tnt_type"]);
                    cpyGetist.Add(obj);
                }

                return cpyGetist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con.Close();
                cmd.Dispose();

            }
        }

    }
}