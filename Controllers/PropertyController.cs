﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class PropertyController : Controller
    {
        IConfiguration _iconfiguration;
        public PropertyController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/addproperty")]
        [HttpPost]
        public IActionResult AddNewCompanyData([FromBody]Object propertyObj)
        {
            try
            {
                AddPropertyRequest request = JsonConvert.DeserializeObject<AddPropertyRequest>(propertyObj.ToString());
                PropertyDal propertyDal = new PropertyDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(propertyDal.AddNewProperty(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }


        [Route("api/properties")]
        [HttpPost]
        public IActionResult GetAllproperties([FromBody]Object getproperties)
        {
            try
            {
                PropertyObj request = JsonConvert.DeserializeObject<PropertyObj>(getproperties.ToString());
                PropertyDal propertyDal = new PropertyDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(propertyDal.GetProperties(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/searchproperties")]
        [HttpPost]
        public IActionResult GetAllsearchproperties([FromBody]Object getproperties)
        {
            try
            {
                GetpropertiesRequest request = JsonConvert.DeserializeObject<GetpropertiesRequest>(getproperties.ToString());
                PropertyDal propertyDal = new PropertyDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(propertyDal.GetSearchProperties(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}