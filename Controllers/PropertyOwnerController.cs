﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI.Controllers
{
    public class PropertyOwnerController : Controller
    {
        IConfiguration _iconfiguration;
        public PropertyOwnerController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/addprtyowner")]
        [HttpPost]
        public IActionResult AddNewPropertyOwnerData([FromBody]Object addPrtyowner)
        {
            try
            {
                AddprtyOwnerRequest request = JsonConvert.DeserializeObject<AddprtyOwnerRequest>(addPrtyowner.ToString());
                PropertyOwnerDal prtyMgrDal = new PropertyOwnerDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(prtyMgrDal.AddNewPropertyOwner(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/getprtyowner")]
        [HttpPost]
        public IActionResult GetPropertyOwnerList([FromBody]Object getprtyowner)
        {
            try
            {
                GetprtyOwnerRequest request = JsonConvert.DeserializeObject<GetprtyOwnerRequest>(getprtyowner.ToString());
                PropertyOwnerDal ownerDal = new PropertyOwnerDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(ownerDal.GetPropertOwnerList(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}