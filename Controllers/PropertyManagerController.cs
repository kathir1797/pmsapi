﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class PropertyManagerController : Controller
    {
        IConfiguration _iconfiguration;
        public PropertyManagerController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/addprtymgr")]
        [HttpPost]
        public IActionResult AddNewCompanyData([FromBody]Object addPrtymgr)
        {
            try
            {
                AddprtymgrRequest request = JsonConvert.DeserializeObject<AddprtymgrRequest>(addPrtymgr.ToString());
                PropertyManagerDal prtyMgrDal = new PropertyManagerDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(prtyMgrDal.AddNewPropertyManager(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/getusers")]
        [HttpPost]
        public IActionResult GetAllUser([FromBody]Object getPrtymgr)
        {
            try
            {
                GetPrtyUserRequest request = JsonConvert.DeserializeObject<GetPrtyUserRequest>(getPrtymgr.ToString());
                PropertyManagerDal prtyMgrDal = new PropertyManagerDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(prtyMgrDal.GetAllUsers(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/getprtymgr")]
        [HttpPost]
        public IActionResult GetAllPrtymgr([FromBody]Object getPrtymgr)
        {
            try
            {
                GetPrtyMgrRequest request = JsonConvert.DeserializeObject<GetPrtyMgrRequest>(getPrtymgr.ToString());
                PropertyManagerDal prtyMgrDal = new PropertyManagerDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(prtyMgrDal.GetAllPropertyManager(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}