﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class ContactController : Controller
    {
        IConfiguration _iconfiguration;
        public ContactController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/addcontact")]
        [HttpPost]
        public IActionResult AddNewContactForm([FromBody]Object contactObj)
        {
            try
            {
                AddContactRequest request = JsonConvert.DeserializeObject<AddContactRequest>(contactObj.ToString());
                ContactsDal contactDal = new ContactsDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(contactDal.AddNewContactForm(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/contacts")]
        [HttpGet]
        public IActionResult GetAllContacts()
        {
            try
            {
                ContactsDal contactDal = new ContactsDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(contactDal.GetContactsList());
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}