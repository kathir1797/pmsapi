﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class DDListItemController : Controller
    {
        IConfiguration _iconfiguration;
        public DDListItemController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/dropdown")]
        [HttpGet]
        public IActionResult GetAllDropDownListItem()
        {
            try
            {
               
                DDListItemDal ddDal = new DDListItemDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(ddDal.GetDropDownHeaderList());
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/ddlist")]
        [HttpPost]
        public IActionResult SelectDDListItem([FromBody]Object DDListName)
        {
            try
            {
                DDListRequest request = JsonConvert.DeserializeObject<DDListRequest>(DDListName.ToString());
                DDListItemDal userDal = new DDListItemDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(userDal.SelectDDItem(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/stateddlist")]
        [HttpPost]
        public IActionResult GetStateDDListItem([FromBody]Object dStateRequest)
        {
            try
            {
                DDStateRequest request = JsonConvert.DeserializeObject<DDStateRequest>(dStateRequest.ToString());
                DDListItemDal userDal = new DDListItemDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(userDal.SelectStateDDItem(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/citylist")]
        [HttpPost]
        public IActionResult GetCityDDListItem([FromBody]Object dStateRequest)
        {
            try
            {
                DDStateRequest request = JsonConvert.DeserializeObject<DDStateRequest>(dStateRequest.ToString());
                DDListItemDal userDal = new DDListItemDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(userDal.SelectCityDDItem(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/countrylist")]
        [HttpPost]
        public IActionResult GetCountryDDListItem([FromBody]Object dStateRequest)
        {
            try
            {
                DDStateRequest request = JsonConvert.DeserializeObject<DDStateRequest>(dStateRequest.ToString());
                DDListItemDal userDal = new DDListItemDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(userDal.SelectCountryDDItem(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/countylist")]
        [HttpPost]
        public IActionResult GetCountyDDListItem([FromBody]Object dStateRequest)
        {
            try
            {
                DDStateRequest request = JsonConvert.DeserializeObject<DDStateRequest>(dStateRequest.ToString());
                DDListItemDal userDal = new DDListItemDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(userDal.SelectCountyDDItem(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}