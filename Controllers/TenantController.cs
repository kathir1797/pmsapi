﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class TenantController : Controller
    {
        IConfiguration _iconfiguration;
        public TenantController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/addtenant")]
        [HttpPost]
        public IActionResult AddNewTenantForm([FromBody]Object tenantObj)
        {
            try
            {
                AddTenantRequest request = JsonConvert.DeserializeObject<AddTenantRequest>(tenantObj.ToString());
                TenantDal tenantDal = new TenantDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(tenantDal.AddNewTenantForm(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/gettenant")]
        [HttpPost]
        public IActionResult GetAllTenant([FromBody]Object getTenant)
        {
            try
            {
                GetTenantRequest request = JsonConvert.DeserializeObject<GetTenantRequest>(getTenant.ToString());
                TenantDal tenantDal = new TenantDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(tenantDal.GetTenantList(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}