﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class ReportsController : Controller
    {
        IConfiguration _iconfiguration;
        public ReportsController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/rptproperties")]
        [HttpPost]
        public IActionResult GetAllReportProperty([FromBody]Object getRptPrty)
        {
            try
            {
                GetRptproperties request = JsonConvert.DeserializeObject<GetRptproperties>(getRptPrty.ToString());
                ReportsDal rptDal = new ReportsDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(rptDal.GetReportPropertyList(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/rptowners")]
        [HttpPost]
        public IActionResult GetAllReportOwners([FromBody]Object getRptOwner)
        {
            try
            {
                GetRptproperties request = JsonConvert.DeserializeObject<GetRptproperties>(getRptOwner.ToString());
                ReportsDal rptDal = new ReportsDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(rptDal.GetReportOwnerList(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/rpttenant")]
        [HttpPost]
        public IActionResult GetAllReportTenant([FromBody]Object getRptTenant)
        {
            try
            {
                GetRptproperties request = JsonConvert.DeserializeObject<GetRptproperties>(getRptTenant.ToString());
                ReportsDal rptDal = new ReportsDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(rptDal.GetReportTenantList(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/rptcontract")]
        [HttpPost]
        public IActionResult GetReportContractList([FromBody]Object getRptContract)
        {
            try
            {
                GetRptproperties request = JsonConvert.DeserializeObject<GetRptproperties>(getRptContract.ToString());
                ReportsDal rptDal = new ReportsDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(rptDal.GetReportContractList(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}