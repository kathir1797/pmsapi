﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class ContractController : Controller
    {
        IConfiguration _iconfiguration;
        public ContractController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/addcontract")]
        [HttpPost]
        public IActionResult AddNewContractForm([FromBody]Object contractObj)
        {
            try
            {
                AddContractRequest request = JsonConvert.DeserializeObject<AddContractRequest>(contractObj.ToString());
                ContractDal contractDal = new ContractDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(contractDal.AddNewContractForm(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/getcontract")]
        [HttpPost]
        public IActionResult GetAllContract([FromBody]Object getcontract)
        {
            try
            {
                GetContractRequest request = JsonConvert.DeserializeObject<GetContractRequest>(getcontract.ToString());
                ContractDal contractDal = new ContractDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(contractDal.GetContractList(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/terminatelease")]
        [HttpPost]
        public IActionResult GetTerminateleaseContract([FromBody]Object terminatelease)
        {
            try
            {
                TerminateLeaseRequest request = JsonConvert.DeserializeObject<TerminateLeaseRequest>(terminatelease.ToString());
                ContractDal contractDal = new ContractDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(contractDal.GetTerminateLease(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}