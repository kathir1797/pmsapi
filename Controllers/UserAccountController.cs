﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class UserAccountController : Controller
    {
        IConfiguration _iconfiguration;
        public UserAccountController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/rootuserlogin")]
        [HttpPost]
        public IActionResult RootUserAccount([FromBody]Object rootUserloginObj) 
        {
            try
            {
                RootloginRequest request = JsonConvert.DeserializeObject<RootloginRequest>(rootUserloginObj.ToString());
                UserAccountDal userDal = new UserAccountDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(userDal.RootUserLogin(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/userpwdreset")]
        [HttpPost]
        public IActionResult UserAccountPasswordReset([FromBody]Object UserPwdReset)
        {
            try
            {
                UserPwdReset request = JsonConvert.DeserializeObject<UserPwdReset>(UserPwdReset.ToString());
                UserAccountDal userDal = new UserAccountDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(userDal.UserPasswordReset(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

    }
}