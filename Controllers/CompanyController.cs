﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PMSAPI
{
    public class CompanyController : Controller
    {
        IConfiguration _iconfiguration;
        public CompanyController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [Route("api/addcompany")]
        [HttpPost]
        public IActionResult AddNewCompanyData([FromBody]Object companyObj)
        {
            try
            {
                AddCompanyRequest request = JsonConvert.DeserializeObject<AddCompanyRequest>(companyObj.ToString());
                CompanyDal companyDal = new CompanyDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(companyDal.AddNewCompany(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }

        [Route("api/companies")]
        [HttpPost]
        public IActionResult GetAllCompanies([FromBody]Object getCompanies)
        {
            try
            {
                GetCtyRequest request = JsonConvert.DeserializeObject<GetCtyRequest>(getCompanies.ToString());
                CompanyDal companyDal = new CompanyDal(_iconfiguration.GetValue<string>("ConnectionStrings:DefaultConnection"));
                return PMSApiSuccessResponse(companyDal.GetCompanies(request));
            }
            catch (Exception ex)
            {
                return this.PMSApiErrorResponse(ex.Message);
            }

        }
    }
}