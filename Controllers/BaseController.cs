﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PMSAPI
{
    public class Controller : Microsoft.AspNetCore.Mvc.Controller
    {
        public Controller() { }
        public IActionResult PMSApiSuccessResponse(object Data = null)
        {
            var obj = new
            {
                success = true,
                errorMessage = "",
                data = Json(Data).Value,
            };

            return Json(obj);
        }
        // return the Error Result for all controller calls
        public IActionResult PMSApiErrorResponse(string ErrorMessage = "", object Data = null)
        {
            var obj = new
            {
                success = false,
                errorMessage = ErrorMessage,
                data = Json(Data).Value
            };

            return Json(obj);
        }
    }
}